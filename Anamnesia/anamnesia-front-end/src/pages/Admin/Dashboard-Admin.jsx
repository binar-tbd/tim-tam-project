import React, { Component } from "react";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import "../../css/Home.css";
import NavigationLogin from "../../components/Navbar-Login";
import axios from "axios";

class Dashboard_Admin extends Component {
  state = {
    published: false,
    news: [],
  };

  componentDidMount() {
    axios.get("/api/news/list-news").then((res) => {
      if (res.status === 200) {
        this.setState({
          news: res.data.news,
        });
      }
    });
  }

  handleApprove = async (event) => {
    const data = {
      id: event.target.value,
    };
    await axios
      .post("/api/news/approve-news", data, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then(() => {
        window.location.reload(false);
      });
  };

  render() {
    return (
      <>
        <NavigationLogin />
        <Container>
          <div className="news-container">
            <div className="news-content">
              <h1>List News</h1>
            </div>
            <div className="news-body">
              <table>
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>News</th>
                    <th>Body</th>
                    <th>Viewer</th>
                    <th>Like</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.news.map((news, index) => {
                    return (
                      <tr>
                        <td>{index + 1}</td>
                        <td>{news.headline}</td>
                        <td>{news.body}</td>
                        <td>{news.viewer}</td>
                        <td>{news.like}</td>
                        <td>
                          {news.status === "0" && (
                            <Link
                              to={`/dashboard-admin/edit-news/${news.id}`}
                              data={news}
                            >
                              <button className="btn btn-info" value={news.id}>
                                Edit
                              </button>
                            </Link>
                          )}
                          {news.status === "3" && (
                            <button
                              className="btn btn-success"
                              onClick={this.handleApprove}
                              value={news.id}
                            >
                              Approve
                            </button>
                          )}
                          {news.status === "3" && <p>Approved</p>}
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </table>
            </div>
          </div>
        </Container>
      </>
    );
  }
}

export default Dashboard_Admin;
