import React, { Component } from "react";
import { Alert, Container } from "react-bootstrap";
import { Link } from "react-router-dom";
import { Redirect } from "react-router";
import axios from "axios";
import "../../css/Home.css";
import NavigationLogin from "../../components/Navbar-Login";
import TextField from "../../components/input/TextField";

class Edit_Admin extends Component {
  state = {
    id: this.props.match.params.id,
    headline: "",
    body: "",
    categories_id: 0,
    categories: [],
    selectedImage: null,
    infoAlert: false,
    variant: "",
    messageAlert: "",
  };

  componentDidMount() {
    console.log(this.state.id);
    axios.get("/api/category/list-category").then((res) => {
      if (res.status === 200) {
        this.setState({
          categories: res.data.categories,
        });
      }
    });

    const data = {
      id: this.state.id,
    };
    axios.post("/api/news/detail-news", data).then((res) => {
      if (res.status === 200) {
        console.log(res.data.news);
        this.setState({
          headline: res.data.news.headline,
          body: res.data.news.body,
          categories_id: res.data.news.categories_id,
        });
      }
    });
  }

  handleFieldOnChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  handleFileOnChange = (event) => {
    this.setState({
      selectedImage: event.target.files[0],
    });
  };

  handleOnApprove = async () => {
    const data = {
      id: this.state.id,
      headline: this.state.headline,
      body: this.state.body,
      categories_id: this.state.categories_id,
    };
    await axios
      .post("/api/news/update-news", data, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then(() => {
        this.setState({
          infoAlert: true,
          variant: "success",
          messageAlert: "Save succesfully!",
        });

        setTimeout(() => {
          this.setState({
            infoAlert: false,
            variant: "",
            messageAlert: "",
          });
        }, 3000);
      });

    return <Redirect to={{ pathname: "/dashboard-admin" }} />;
  };

  render() {
    return (
      <>
        <NavigationLogin />
        <Container>
          <div className="news-container">
            {this.state.infoAlert && (
              <Alert variant={this.state.variant}>
                {this.state.messageAlert}
              </Alert>
            )}
            <div className="news-content">
              <h1>Edit News</h1>
              <Link to="/dashboard-admin">
                <button className="btn btn-secondary">Back</button>
              </Link>
            </div>
            <div className="news-body">
              <form onApprove={this.handleOnApprove}>
                <TextField
                  name="headline"
                  onChange={this.handleFieldOnChange}
                  placeholder="Headline"
                  value={this.state.headline}
                />
                <br></br>
                <TextField
                  name="body"
                  onChange={this.handleFieldOnChange}
                  placeholder="Body"
                  value={this.state.body}
                />
                <br></br>
                <select
                  className="form-control"
                  name="categories_id"
                  onChange={this.handleFieldOnChange}
                  options={this.state.categories}
                  value={this.state.categories_id}
                >
                  <option value="">Pilih Kategori</option>
                  {this.state.categories.map((category) => {
                    return (
                      <option value={category.id}>
                        {category.category_name}
                      </option>
                    );
                  })}
                </select>
                <br></br>
                <input
                  type="Approve"
                  className="btn btn-success"
                  value="Approve"
                />
              </form>
            </div>
          </div>
        </Container>
      </>
    );
  }
}

export default Edit_Admin;
