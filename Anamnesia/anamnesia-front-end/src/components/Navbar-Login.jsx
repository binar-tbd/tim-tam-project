import React, { Component } from "react";
import "../css/Navbar.css";
import Hamburger from "./Hamburger";
import logo from "../assets/logo_anamnesia.png";
import Button from "@material-ui/core/Button";
import { withRouter } from "react-router-dom";
class Navigation extends Component {
    state = {
        redirectPage: false,
        token: localStorage.getItem("token"),
    };

    logout = () => {
        this.setState({
            redirectPage: true,
            token: localStorage.removeItem("token"),
        });

        this.handleRedirectPage();
    };

    handleRedirectPage = () => {
        if (this.state.redirectPage) {
            this.props.history.push("/login");
        }
    };

    render() {
        return (
            <>
                <div className="nav">
                    <div className="icon">
                        <Hamburger />
                    </div>
                    <div className="logo">
                        <img src={logo} height="80%" alt="logo" />
                    </div>
                    <div className="components">
                        <div class="navigation">
                            <Button color="inherit">Profile</Button>
                            <Button onClick={this.logout} color="inherit">
                                Logout
                            </Button>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default withRouter(Navigation);
