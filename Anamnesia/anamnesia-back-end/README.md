Untuk menjalankan back-end lakukan step berikut
1. npm install
2. sesuaikan username, password dan database pada file config/config.json
3. sequelize db:migrate
4. sequelize db:seed:all
5. jalankan BE pada terminal dengan perintah "node index.js"