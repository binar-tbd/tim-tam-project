"use strict";

const bcrypt = require("bcrypt");
const password = "123456";
const hash = bcrypt.hashSync(password, 10);

module.exports = {
    up: async (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert(
            "users",
            [
                {
                    email: "admin@gmail.com",
                    password: hash,
                    full_name: "admin",
                    gender: "1",
                    address: "Jakarta",
                    phone: "0",
                    role: "1",
                    createdAt: new Date(),
                    updatedAt: new Date(),
                },
            ],
            {}
        );
    },

    down: async (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete("users", null, {});
    },
};
