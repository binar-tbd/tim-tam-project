const { categories } = require("../../../models");

const listCategoryAction = function (req, res, next) {
  categories
    .findAll()
    .then((categories) => {
      res.json({
        categories,
      });
    })
    .catch((err) => next(err));
};

module.exports = { listCategoryAction };
