const { news } = require("../../../models");

const listNewsAction = function (req, res, next) {
  news
    .findAll()
    .then((news) => {
      res.json({
        news,
      });
    })
    .catch((err) => next(err));
};

const updateNewsAction = function (req, res, next) {
  news
    .updateNews(req.body)
    .then((news) => {
      res.json({
        headline: news.headline,
        body: news.body,
        categories_id: news.categories_id,
        status: news.status,
        viewer: news.viewer,
        like: news.like,
        users_id: news.users_id,
      });
    })
    .catch((err) => next(err));
};

const approveNewsAction = function (req, res, next) {
  news
    .approveNews(req.id)
    .then((news) => {
      res.json(news);
    })
    .catch((err) => next(err));
};

module.exports = {
  listNewsAction,
  updateNewsAction,
  approveNewsAction,
};
