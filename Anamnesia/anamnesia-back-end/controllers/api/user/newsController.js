const { news } = require("../../../models");

const listNewsAction = function (req, res, next) {
    news.findAll({
        include: ["categoriesId"],
    })
        .then((news) => {
            res.json({
                news,
            });
        })
        .catch((err) => next(err));
};

const detailNewsAction = function (req, res, next) {
    news.detailNews(req.body.id)
        .then((news) => {
            res.json({
                news,
            });
        })
        .catch((err) => next(err));
};

const createNewsAction = function (req, res, next) {
    console.log("body");
    console.log(req.body);
    news.createNews(req.body)
        .then((news) => {
            res.json({
                headline: news.headline,
                body: news.body,
                categories_id: news.categories_id,
                status: news.status,
                viewer: news.viewer,
                like: news.like,
                users_id: news.users_id,
            });
        })
        .catch((err) => next(err));
};

const draftNewsAction = function (req, res, next) {
    console.log(req.body);
    news.draftNews(req.body)
        .then((news) => {
            res.status(200).json({
                headline: news.headline,
                body: news.body,
                categories_id: news.categories_id,
                status: news.status,
                viewer: news.viewer,
                like: news.like,
                users_id: news.users_id,
            });
        })
        .catch((err) => next(err));
};

const updateNewsAction = function (req, res, next) {
    news.updateNews(req.body)
        .then((news) => {
            res.json({
                headline: news.headline,
                body: news.body,
                categories_id: news.categories_id,
                status: news.status,
                viewer: news.viewer,
                like: news.like,
                users_id: news.users_id,
            });
        })
        .catch((err) => next(err));
};

const publishNewsAction = function (req, res, next) {
    news.publishNews(req.body.id)
        .then((news) => {
            res.json(news);
        })
        .catch((err) => next(err));
};

module.exports = {
    listNewsAction,
    detailNewsAction,
    createNewsAction,
    draftNewsAction,
    updateNewsAction,
    publishNewsAction,
};
