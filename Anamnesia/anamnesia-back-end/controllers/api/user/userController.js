const { users } = require("../../../models");

const getUserDetail = (req, res, next) => {
  users
    .findOne({
      where: { id: req.params.id },
    })
    .then((user) => res.json(user))
    .catch((err) => res.send(err));
};

module.exports = { getUserDetail };
