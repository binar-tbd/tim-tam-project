const { news } = require('../../../models');

const listNewsAction = function (req, res, next) {
  news
    .findAll({ include: ['categoriesId'] })
    .then((news) => {
      res.json({ news });
    })
    .catch((err) => next(err));
};

const detailNewsAction = function (req, res, next) {
  news.detailNews(req.body.id)
      .then((news) => {
          res.json({
              news,
          });
      })
      .catch((err) => next(err));
};

const likeNewsAction = function (req, res, next) {
  news.likeNews(req.body)
      .then((news) => {
          res.json({
              like: news.like
          });
      })
      .catch((err) => next(err));
};

const viewNewsAction = function (req, res, next) {
  news.viewNews(req.body)
      .then((news) => {
          res.json({
              viewer: news.viewer,
          });
      })
      .catch((err) => next(err));
};

module.exports = {
  listNewsAction,
  detailNewsAction,
  likeNewsAction,
  viewNewsAction
};
