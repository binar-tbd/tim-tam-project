const { users } = require("../models");

const registerAction = (req, res) => {
  users
    .register(req.body)
    .then(() => res.send("Register Success"))
    .catch(() => res.send("Check Your Data"));
};

const loginAction = (req, res, next) => {
  users
    .userAuthenticate(req.body)
    .then((user) => {
      res.json({
        auth: true,
        token: user.generateToken(),
        user: user,
      });
    })
    .catch((err) => res.send(err));
};

module.exports = { registerAction, loginAction };
