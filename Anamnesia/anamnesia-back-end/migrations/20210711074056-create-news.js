"use strict";
module.exports = {
    up: async (queryInterface, Sequelize) => {
        await queryInterface.createTable("news", {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER,
            },
            headline: {
                type: Sequelize.STRING,
            },
            body: {
                type: Sequelize.TEXT,
            },
            image: {
                type: Sequelize.STRING,
            },
            categories_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: "categories",
                    key: "id",
                    onUpdate: "cascade",
                    onDelete: "cascade",
                },
            },
            status: {
                type: Sequelize.STRING,
            },
            viewer: {
                type: Sequelize.INTEGER,
            },
            like: {
                type: Sequelize.INTEGER,
            },
            users_id: {
                type: Sequelize.INTEGER,
                references: {
                    model: "users",
                    key: "id",
                    onUpdate: "cascade",
                    onDelete: "cascade",
                },
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE,
            },
        });
    },
    down: async (queryInterface, Sequelize) => {
        await queryInterface.dropTable("news");
    },
};
