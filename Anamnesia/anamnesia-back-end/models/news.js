"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
    class news extends Model {
        static associate(models) {
            models.news.belongsTo(models.categories, {
                foreignKey: "categories_id",
                as: "categoriesId",
            });

            models.news.belongsTo(models.users, {
                foreignKey: "users_id",
                as: "usersId",
            });
        }

        static createNews = function ({
            headline,
            body,
            categories_id,
            users_id,
        }) {
            return this.create({
                headline: headline,
                body: body,
                categories_id: categories_id,
                status: "1",
                viewer: 0,
                like: 0,
                users_id: users_id,
            });
        };

        static draftNews = function ({
            headline,
            body,
            categories_id,
            users_id,
        }) {
            return this.create({
                headline: headline,
                body: body,
                categories_id: categories_id,
                status: "0",
                viewer: 0,
                like: 0,
                users_id: users_id,
            });
        };

        static detailNews = function (id) {
            let data = this.findOne({
                where: { id: id },
            });

            return data;
        };

        static updateNews = async function ({
            id,
            headline,
            body,
            image,
            categories_id,
            users_id,
        }) {
            let data = await this.update(
                {
                    headline: headline,
                    body: body,
                    image: image,
                    categories_id: categories_id,
                    status: "2",
                    users_id: users_id,
                },
                {
                    where: { id: id },
                }
            );
            return data;
        };

        static publishNews = function (id) {
            let data = this.update(
                {
                    status: "2",
                },
                {
                    where: { id: id },
                }
            );

            return data;
        };

        static detailNews = function (id) {
            let data = this.findOne({
                where: { id: id },
                include: ["categoriesId"],
            });
            console.log(data);
            return data;
        };

        static likeNews = async function ({ id, like }) {
            let data = await this.update(
                {
                    like: like + 1,
                },
                {
                    where: { id: id },
                }
            );
            return data;
        };

        static viewNews = async function ({ id, viewer }) {
            let data = await this.update(
                {
                    viewer: viewer + 1,
                },
                {
                    where: { id: id },
                }
            );
            return data;
        };

		static approveNews = function (id) {
            let data = this.update(
                {
                    status: "3",
                },
                {
                    where: { id: id },
                }
            );

            return data;
        };
    }
    news.init(
        {
            headline: DataTypes.STRING,
            body: DataTypes.TEXT,
            image: DataTypes.STRING,
            categories_id: {
                type: DataTypes.INTEGER,
                field: "categories_id",
            },
            status: DataTypes.STRING,
            viewer: DataTypes.INTEGER,
            like: DataTypes.INTEGER,
            users_id: {
                type: DataTypes.INTEGER,
                field: "users_id",
            },
        },
        {
            sequelize,
            modelName: "news",
        }
    );
    return news;
};
