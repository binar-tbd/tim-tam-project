"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

module.exports = (sequelize, DataTypes) => {
  class users extends Model {
    static associate(models) {
      models.users.hasMany(models.news, {
        foreignKey: "id",
      });
    }

    static #encrypt = (password) => bcrypt.hashSync(password, 10);

    static register = ({
      email,
      password,
      full_name,
      gender,
      address,
      phone,
      role,
    }) => {
      const encryptedPassword = this.#encrypt(password);
      const userRole = role !== "admin" ? "0" : "1";
      const codeGender = gender === "male" ? "1" : "0";
      return this.create({
        email,
        password: encryptedPassword,
        full_name,
        gender: codeGender,
        address,
        phone,
        role: userRole,
      });
    };

    generateToken = () => {
      const payLoad = {
        id: this.id,
        email: this.email,
      };

      const token = jwt.sign(payLoad, "anamnesia", { expiresIn: 300 });
      return token;
    };

    checkPassword = (password) => bcrypt.compareSync(password, this.password);

    static userAuthenticate = async ({ email, password }) => {
      try {
        const user = await this.findOne({ where: { email } });

        if (!user || !user.checkPassword(password)) {
          return Promise.reject("Invalid Username & Password !!");
        }

        return Promise.resolve(user);
      } catch (error) {
        return Promise.reject(error);
      }
    };
  }

  users.init(
    {
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      full_name: DataTypes.STRING,
      gender: DataTypes.STRING,
      address: DataTypes.STRING,
      phone: DataTypes.STRING,
      role: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "users",
    }
  );
  return users;
};
