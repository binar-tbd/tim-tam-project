var express = require('express');
var router = express.Router();
const landingpageController = require('../../../controllers/api/landingpage/landingpagecontroller');

router.get('/listnews', landingpageController.listNewsAction);
router.post('/detailnews', landingpageController.detailNewsAction);
router.post('/likenews', landingpageController.likeNewsAction);
router.post('/viewnews', landingpageController.viewNewsAction);

module.exports = router;
