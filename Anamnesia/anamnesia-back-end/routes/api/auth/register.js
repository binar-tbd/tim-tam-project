const express = require("express");
const register = express.Router();
const authController = require("../../../controllers/authController");

register.get("/", (req, res) => {
  res.send({ message: "Success Register" });
});

register.post("/", authController.registerAction);

module.exports = register;
