const express = require("express");
const login = express.Router();
const authController = require("../../../controllers/authController");

login.post("/", authController.loginAction);

module.exports = login;
