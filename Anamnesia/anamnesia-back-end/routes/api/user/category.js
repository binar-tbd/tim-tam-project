var express = require("express");
var router = express.Router();
const categoryController = require("../../../controllers/api/user/categoryController");

router.get("/list-category", categoryController.listCategoryAction);

module.exports = router;
