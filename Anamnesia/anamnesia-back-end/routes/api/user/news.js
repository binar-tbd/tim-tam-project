var express = require("express");
var router = express.Router();
const newsController = require("../../../controllers/api/user/newsController");
const restrictJwt = require("../../../middlewares/restrict-jwt");

router.get("/list-news", newsController.listNewsAction);
router.post("/detail-news", newsController.detailNewsAction);
router.post("/create-news", newsController.createNewsAction);
router.post("/draft-news", newsController.draftNewsAction);
router.post("/update-news", newsController.updateNewsAction);
router.post("/publish-news", newsController.publishNewsAction);

module.exports = router;
