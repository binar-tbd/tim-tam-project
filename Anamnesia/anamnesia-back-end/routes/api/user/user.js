const express = require("express");
const user = express.Router();
const userController = require("../../../controllers/api/user/userController");
const restrictJwt = require("../../../middlewares/restrict-jwt");

user.get("/:id", restrictJwt, userController.getUserDetail);

module.exports = user;
