var express = require("express");
var router = express.Router();
const newsController = require("../../../controllers/api/admin/newsController");

router.get("/list-news", newsController.listNewsAction);
router.post("/update-news/", newsController.updateNewsAction);
router.get("/approve-news", newsController.approveNewsAction);

module.exports = router;
