const express = require("express");
const router = express.Router();
const apiAdminNewsRouter = require("./admin/news");
const apiAdminCategoryRouter = require("./admin/category");
const apiRegister = require("../api/auth/register");
const apiLogin = require("../api/auth/login");
const apiUser = require("../api/user/user");
const apiUserNewsRouter = require("./user/news");
const apiUserCategoryRouter = require("./user/category");
const apiNewsRouter = require('./landingpage/landingpage');

router.use("/register", apiRegister);
router.use("/login", apiLogin);
router.use("/user", apiUser);
router.use("/api/news", apiUserNewsRouter);
router.use("/api/category", apiUserCategoryRouter);
router.use('/api/landingpage', apiNewsRouter);
router.use("/api/admin/news", apiAdminNewsRouter);
router.use("/api/admin/category", apiAdminCategoryRouter);

module.exports = router;
