const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const { users } = require("../models");

passport.use(
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: "anamnesia",
    },
    (payLoad, done) => {
      users
        .findByPk(payLoad.id)
        .then((user) => done(null, user))
        .catch((err) => done(err, false));
    }
  )
);

module.exports = passport;
