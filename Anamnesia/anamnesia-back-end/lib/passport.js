const passport = require("passport");
const { Strategy: JwtStrategy, ExtractJwt } = require("passport-jwt");
const { users } = require("../models");

const option = {
  jwtFromRequest: ExtractJwt.fromHeader("authorization"),
  secretOrKey: "amnesia",
};

passport.use(
  new JwtStrategy(option, async (payload, done) => {
    users
      .findByPk(payload.id)
      .then((user) => done(null, user))
      .cacth((err) => done(err, false));
  })
);

module.exports = passport;
